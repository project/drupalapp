<div class="<?php if (isset($classes)) print $classes; ?>" id="<?php print $block_html_id; ?>"<?php print $attributes; ?>>
<div class="dna-box dna-post">
<div class="dna-box-body dna-post-body">
<article class="dna-post-inner dna-article">
<?php print render($title_prefix); ?>
<?php if ($block->subject): ?>
<h2 class="dna-postheader"><?php print $block->subject ?></h2>
<?php endif;?>
<?php print render($title_suffix); ?>
<div class="dna-postcontent">
<div class="dna-article content">
<?php print $content; ?>
</div>
</div>
<div class="cleared"></div>
</article>
<div class="cleared"></div>
</div>
</div>
</div>
